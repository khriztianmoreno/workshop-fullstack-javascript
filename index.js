/**
 * Export the application
 * @author: Cristian Moreno Zulauaga <khriztianmoreno@gmail.com>
 */
module.exports = require('./app');
